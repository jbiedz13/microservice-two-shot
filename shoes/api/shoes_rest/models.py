from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique= True)
    bin_number = models.PositiveSmallIntegerField()
    closet_name = models.CharField(max_length=100, null= True)



class Shoe(models.Model):
    nickname = models.CharField(max_length=100, null=True)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="bins",
        on_delete=models.CASCADE,
        null=True
    )

    def get_api_url(self):
        return reverse("api_shoe_detail", kwargs={"id":self.pk})



"""
Create a VO for colors and/or model name?
if no nickname default to model name
"""
