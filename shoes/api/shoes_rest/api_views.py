from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Shoe, BinVO
# from .acl import get_photo
import requests



class ShoeListEncoder(ModelEncoder):
    model= Shoe
    properties = [
        "nickname",
        "model_name",
    ]

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "bin_number",
        "closet_name",
    ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "nickname",
        "bin",
    ]

    encoders= {
        "bin": BinVOEncoder(),
        "href": ShoeListEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes= Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes}, encoder=ShoeListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href= bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid bin",},
                status= 400,
            )
        # content["picture_url"] = get_photo(content["color"], content["model_name"])
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe= False
        )


@require_http_methods(["GET", "DELETE"])
def api_shoe_detail(request, id):
    if request.method =="GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe= False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"delete shoe:": count > 0})
