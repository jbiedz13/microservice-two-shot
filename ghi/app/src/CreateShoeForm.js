import React from 'react'
import { useEffect, useState} from 'react'

export default function CreateShoeForm() {
    const [formData, setFormData] = useState({
        nickname : "",
        model_name: "",
        color: "",
        manufacturer: "",
        bin: "",
        picture_url: "",
    })

    // handle changes to form state/ input from user
    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setFormData({
            ...formData,
            [inputName]:value,
        })
    }

        // get bins to populate dropdown
        const [bins, setBins] = useState([])
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/bins/'
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setBins(data.bins)
            }
        }

        useEffect(() => {fetchData()}, [])

        // handle form submission
        const handleSubmit = async (event) => {
            event.preventDefault()
            const url = 'http://localhost:8080/api/shoes/'
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const response = await fetch(url, fetchConfig)
            if (response.ok) {
                setFormData({
                    nickname : "",
                    model_name: "",
                    color: "",
                    manufacturer: "",
                    bin: "",
                    picture_url: "",
                })
            }
        }

        return (
            <div className="row">
                <div>
                  <div className="shadow p-4 mt-4">
                    <h2>Add Shoes</h2>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        {/* <!-- nickname --> */}
                      <div className="form-floating mb-3">
                        <input onChange ={handleFormChange} value ={formData.nickname} placeholder="nickname" required type="text" id="nickname" name="nickname" className="form-control" />
                        <label htmlFor="nickname">Nickname</label>
                      </div>
                      {/* <!-- manufacturer --> */}
                      <div className="form-floating mb-3">
                        <input onChange ={handleFormChange} value ={formData.manufacturer} placeholder="manufacturer" required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                        <label htmlFor="manufacturer">Manufacturer</label>
                      </div>
                      {/* <!-- model_name --> */}
                      <div className="form-floating mb-3">
                        <input onChange ={handleFormChange} value ={formData.model_name} placeholder="model" required type="text" id="model_name" name="model_name" className="form-control" />
                        <label htmlFor="model_name">Model</label>
                      </div>
                      {/* <!-- Color --> */}
                      <div className="form-floating mb-3">
                        <input onChange ={handleFormChange} value ={formData.color} placeholder="color" required type="text" id="color" name="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                      </div>
                      {/* <!-- picture_url --> */}
                      <div className="form-floating mb-3">
                        <input onChange ={handleFormChange} value ={formData.picture_url} placeholder="picture" required type="text" id="picture_url" name="picture_url" className="form-control" />
                        <label htmlFor="picture_url">Picture URL</label>
                      </div>
                      {/* <!-- bin --> */}
                      <div className="mb-3">
                        <select onChange ={handleFormChange} value ={formData.bin} required id="bin" name="bin" className="form-select">
                          <option value="">Closet / Bin</option>
                          {bins.map(bin => {
                            console.log(`bin: ${bin}`)
                            return (
                            <option value={bin.href} key={bin.href}>
                                {bin.closet_name} bin {bin.bin_number}
                            </option>
                            )
                            })}
                        </select>
                      </div>
                      <button className="btn btn-primary">Create</button>
                    </form>
                  </div>
                </div>
                </div>
            )
    }
