import React, { useState, useEffect } from 'react'
import HatsForm from './HatsForm'

function HatsList() {
    const [Hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/')

        if (response.ok) {
            const data = await response.json()
            setHats(data.hats)
        }
    }

    useEffect(() => {
        getData()
    }, [])
    return (
        <>
        <div>
        <div>
        <HatsForm />
        </div>

        <div className='col' > Hats
        <table className="table table-stiped">
            <thead>
                <tr>
                    <th>Style Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>ID</th>
                </tr>
            </thead>
            <tbody>
                {Hats.map(hat => {
                    return (
                    <tr>
                        <td>{hat.style_name}</td>
                        <td>{hat.fabric}</td>
                        <td>{hat.color}</td>
                        <div>
                        <img style={{ width: 100, height: 100 }} src={hat.picture_url} className="img-thumbnail" alt="..."/>
                        </div>
                        <td>{hat.id}</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    </div>
    </div>

    </>

    )
  }

export default HatsList
