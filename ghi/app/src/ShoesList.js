import React, { useState, useEffect } from 'react'
import CreateShoeForm from './CreateShoeForm'


function ShoesColumn(props) {
    return (
        <div className='col'>
            {props.list.map(data => {
                return (
                    <div className="card">
                        <img src={data.picture_url} className="card-img-top" alt="" />
                        <div className="card-body">
                            <h5 className="card-title">{data.nickname}</h5>
                            <p className="card-text">{`Manufacturer: ${data.manufacturer}`}</p>
                            <p className="card-text">{`Model: ${data.model_name}`}</p>
                        </div>
                        <div className="card-footer">
                        <p className="card-text">{data.bin.closet_name}</p>
                        <p className="card-text">{`Bin ${data.bin.bin_number}`}</p>
                        </div>
                    </div>
                )
            })}
        </div>
    )
  }

  const ShoesPage = (props) => {
    // get data from request to shoes list api
    const [shoeColumns, setShoesColumns] = useState([[],[],[],[]])
    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/"
        try {
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                const requests = []
                for (let shoe of data.shoes) {
                    const detailUrl = `http://localhost:8080${shoe.href}`
                    requests.push(fetch(detailUrl))
                }
                const responses = await Promise.all(requests)
                const columns = [[],[],[],[]]
                let i = 0
                for (const shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const details = await shoeResponse.json()
                        columns[i].push(details)
                        i++
                        if (i>3) {
                            i=0
                        }
                    } else {
                        console.error(shoeResponse)
                    }
                }
                setShoesColumns(columns)
            }
        } catch (e) {
            console.error(e)
        }
    }
    useEffect(() => {fetchData()}, [])
    return (
        <>
        <div className="container">
            <div className="row shadow p-4 mt-4">
                <h2>My Shoes</h2>
                {shoeColumns.map((shoeList, index) => {
                return (
                <ShoesColumn key={index} list={shoeList} />
                );
            })}
            </div>
      </div>
      <div>
            <CreateShoeForm />
        </div>
      </>
    )
  }

export default ShoesPage
