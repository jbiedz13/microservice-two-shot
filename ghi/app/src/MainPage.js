import ShoesPage from "./ShoesList";
import { NavLink } from "react-router-dom";

function MainPage() {
  return (
    <>
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
    </div>
    <div className="container">
      <div className="row">
      <div className='col'>
        <div className="card">
          <h1>Hats</h1>
          <NavLink to="hats/">
          <img src="http://www.clker.com/cliparts/b/0/3/7/1206576685673534961rygle_Hat_Outline.svg" className="image-thumbnail rounded float-start" alt=""/>
          </NavLink>
        </div>
      </div>
      <div className="col">
      <div className="card">
          <h1>Shoes</h1>
          <NavLink to="shoes/" >
            <img src="https://images.vexels.com/media/users/3/234060/isolated/lists/dfb70443547438a08eb531c44ca3eb4e-low-top-sneaker-side-view-stroke.png" className="image-thumbnail rounded float-start" alt="" />
          </NavLink>
        </div>
      </div>
      </div>
      </div>
    </>
  );
}

export default MainPage;
