import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesPage from './ShoesList';
import HatsList from './HatsList';
import HatsForm from './HatsForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes/" element={<ShoesPage />}></Route>
          <Route path="hats">
            <Route index element={<HatsList />} />
            <Route path="new" element={<HatsForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
