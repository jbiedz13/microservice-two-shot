import React, { useEffect, useState } from 'react';

function HatsForm () {

    const [locations, setLocations] = useState([])

    const [style_name, setStyleName] = useState('')
    const handleStyleNameChange = (event) => {
        const value = event.target.value
        setStyleName(value)
    }

    const [fabric, setFabric] = useState('')
    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
    }


    const [color, setColor] = useState('')
    const handleColor = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const [pictureUrl, setPictureUrl] = useState('')
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)

    }

    const [location, setLocation] = useState('')
    const handleLocation = (event) => {
        const value = event.target.value
        setLocation(value)

    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.style_name = style_name
        data.fabric = fabric
        data.color = color
        data.picture_url = pictureUrl



        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();


            setStyleName('')
            setFabric('')
            setColor('')
            setPictureUrl('')
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)
    }
}

    useEffect(() => {
        fetchData();
    }, []);


        return (
            <div className="row">
                <div>
                  <div className="shadow p-4 mt-4">
                    <h2>Add Hats</h2>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                      <div className="form-floating mb-3">
                        <input onChange ={handleStyleNameChange} value ={style_name} placeholder="style_name" required type="text" id="style_name" name="style_name" className="form-control" />
                        <label htmlFor="style_name">Style Name</label>
                      </div>
                      <div className="form-floating mb-3">
                        <input onChange ={handleFabricChange} value ={fabric} placeholder="fabric" required type="text" id="fabric" name="fabric" className="form-control" />
                        <label htmlFor="fabric">Fabric</label>
                      </div>
                      <div className="form-floating mb-3">
                        <input onChange ={handleColor} value ={color} placeholder="picture_url" required type="text" id="model_name" name="model_name" className="form-control" />
                        <label htmlFor="model_name">Color</label>
                      </div>
                      <div className="form-floating mb-3">
                        <input onChange ={handlePictureUrlChange} value ={pictureUrl} placeholder="picture_url" required type="text" id="picture_url" name="picture_url" className="form-control" />
                        <label htmlFor="picture_url">Picture Url</label>
                      </div>
                      <div className="mb-3">
                        <select onChange ={handleLocation} value ={location} required id="location" name="location" className="form-select">
                          <option value="">Closet / Shelf</option>
                          {locations.map(location => {
                            console.log(`location: ${location}`)
                            return (
                            <option value={location.href} key={location.href}>
                                {location.closet_name} location {location.shelf_name}
                            </option>
                            )
                            })}
                        </select>
                      </div>
                      <button className="btn btn-primary">Create</button>
                    </form>
                  </div>
                </div>
                </div>
            )
    }

export default HatsForm
